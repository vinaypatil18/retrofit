package com.vinay.passioneer;

import java.util.List;

import com.vinay.passioneer.model.Reviews;
import com.vinay.passioneer.model.Reviews.ReviewsResponse;
import com.vinay.passioneer.service.TMDB_Service;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class TestRetrofit {
	public static void main(String[] args) {
		
		String sortBy = "popularity.desc";
		String apiKey = "adeee82aa4877de3d9d94f33cb53396a";
		int movieID = 102899;
		
		try {
			//get popular movies
			/*List<MovieModel> movieModels =  WebService.fetchPopularMovies(sortBy,apiKey);
			if(movieModels != null) {
				for(MovieModel movieModel : movieModels) {
					System.out.println(movieModel.getOriginal_title());
				}
			}*/
			
			// get trailers
			/*
			Call<TrailersResponse> call = WebService.getMovieTrailers(movieID,apiKey);
			System.out.println(System.currentTimeMillis());
			call.enqueue(new Callback<TrailersResponse>() {
				
				@Override
				public void onResponse(Response<TrailersResponse> response, Retrofit retrofit) {
					if (response.isSuccess()) {
						System.out.println(response.code());
						List<Trailers> trailers = response.body().getTrailers();
						System.out.println(trailers.size());
					}
				}
				
				@Override
				public void onFailure(Throwable arg0) {
					// TODO Auto-generated method stub
					
				}
			});*/
			
			
			Call<ReviewsResponse> call = TMDB_Service.getMovieReviews(movieID, apiKey);
			call.enqueue(new Callback<ReviewsResponse>(){

				@Override
				public void onFailure(Throwable arg0) {
					
				}

				@Override
				public void onResponse(Response<ReviewsResponse> response, Retrofit retrofit) {
					if (response.isSuccess()) {
						System.out.println(response.code());
						List<Reviews> reviews = response.body().getReviews();
						for(Reviews review : reviews) {
							System.out.println(review.getId());
						}
					}
				}
				
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
}
