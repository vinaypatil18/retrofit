package com.vinay.passioneer.resources;

import com.vinay.passioneer.model.MovieModel.PopularMoviesResponse;
import com.vinay.passioneer.model.Reviews.ReviewsResponse;
import com.vinay.passioneer.model.Trailers.TrailersResponse;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;

public interface TMDB_Endpoints {

	@GET("discover/movie")
	Call<PopularMoviesResponse> getPopularMovies(@Query("sort_by") String sortBy, @Query("api_key") String apiKey);

	@GET("movie/{id}/videos")
	Call<TrailersResponse> getMovieTrailers(@Path("id") int movieID, @Query("api_key") String apiKey);
	
	@GET("movie/{id}/reviews")
	Call<ReviewsResponse> getMovieReviews(@Path("id") int movieID, @Query("api_key") String apiKey);

}
