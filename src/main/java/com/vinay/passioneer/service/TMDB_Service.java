package com.vinay.passioneer.service;

import java.io.IOException;
import java.util.List;

import com.vinay.passioneer.model.MovieModel;
import com.vinay.passioneer.model.MovieModel.PopularMoviesResponse;
import com.vinay.passioneer.model.Reviews.ReviewsResponse;
import com.vinay.passioneer.model.Trailers.TrailersResponse;
import com.vinay.passioneer.resources.TMDB_Endpoints;

import retrofit.Call;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

public class TMDB_Service {

	private static Retrofit retrofit;
	private static TMDB_Endpoints service;
	private static final String BASE_URI = "http://api.themoviedb.org/3/";

	static {
		System.out.println("Building Retrofit");
		retrofit = new Retrofit.Builder().baseUrl(BASE_URI).addConverterFactory(GsonConverterFactory.create()).build();
		service = retrofit.create(TMDB_Endpoints.class);
	}

	public static List<MovieModel> fetchPopularMovies(String sortBy, String apiKey) throws IOException {
		System.out.println("Inside fetchPopularMovies");
		Call<PopularMoviesResponse> call = service.getPopularMovies(sortBy, apiKey);
		Response<PopularMoviesResponse> response = call.execute();
		PopularMoviesResponse responseModel = response.body();
		return responseModel.getMovieModels();
	}

	// asynchronous call will be made by the caller
	public static Call<TrailersResponse> getMovieTrailers(int movieID, String apiKey) {
		Call<TrailersResponse> call = service.getMovieTrailers(movieID, apiKey);
		return call;
	}
	
	public static Call<ReviewsResponse> getMovieReviews(int movieID, String apiKey) {
		Call<ReviewsResponse> call = service.getMovieReviews(movieID, apiKey);
		return call;
	}
}
